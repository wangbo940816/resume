<h1 align="center">Welcome to resume 👋</h1>
<p>
  <a href="https://www.npmjs.com/package/resume" target="_blank">
    <img alt="Version" src="https://img.shields.io/npm/v/resume.svg">
  </a>
</p>

> 恪晨的在线简历模版

### 🏠 [Homepage](https://github.com/BoWang816/resume)

### ✨ [Demo](https://resume.wangbooweb.site)

## Install

```sh
npm install
```

## Usage

```sh
npm run start
```

You can change data.js, update your resume!

## Author

👤 **bo.wang**

* Website: https://blog.wangboweb.site
* Github: [@BoWang816](https://github.com/BoWang816)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://github.com/BoWang816/resume/issues). 

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_s
