/**
 * index.js
 * @author wangbo
 * @since 2021/1/14
 */
import styles from './index.module.less';
import CommonModule from "../module";

export default function Skill() {
    const title = '职业技能';
    const id = 'skill';
    return (
        <CommonModule id={id} title={title}>
            <div className={styles.webSkill}>
                <div className={styles.skillItem}>
                    <header className={styles.skillTitle}>web</header>
                    <ul className={styles.skillDesc}>
                        <li>熟知JS基本语法与知识，掌握最新ES6语法与API，熟练TypeScript</li>
                        <li>开发多个浏览器插件<a href="https://chrome.google.com/webstore/detail/%E7%99%BE%E5%BA%A6%E6%96%87%E5%BA%93%E5%85%8D%E8%B4%B9%E4%B8%8B%E8%BD%BD/imjoocoajfjgnabmlbgpcnpieibibhmd?hl=zh-CN&amp;authuser=0" hl="zh-CN" authuser="0">百度文库下载</a>、<a href="https://chrome.google.com/webstore/detail/%E4%BD%8E%E4%BB%B7%E9%9B%B7%E8%BE%BE/hgbfabcmmeahnfcodhpompojenmpoiam?hl=zh-CN" hl="zh-CN">低价雷达</a>、<a href="https://chrome.google.com/webstore/detail/b%E7%AB%99%E8%A7%86%E9%A2%91%E4%B8%8B%E8%BD%BD/ohfnohibneicledlcolmfeochkmbecjd?hl=zh-CN&amp;authuser=0" hl="zh-CN" authuser="0">B站视频下载</a></li>
                        <li>熟知JS基本语法与知识，掌握最新ES6语法与API，熟练TypeScript</li>
                    </ul>
                </div>
                <div className={styles.skillItem}>
                    <header className={styles.skillTitle}>web</header>
                    <ul className={styles.skillDesc}>
                        <li>熟知JS基本语法与知识，掌握最新ES6语法与API，熟练TypeScript</li>
                        <li>开发多个浏览器插件<a href="https://chrome.google.com/webstore/detail/%E7%99%BE%E5%BA%A6%E6%96%87%E5%BA%93%E5%85%8D%E8%B4%B9%E4%B8%8B%E8%BD%BD/imjoocoajfjgnabmlbgpcnpieibibhmd?hl=zh-CN&amp;authuser=0" hl="zh-CN" authuser="0">百度文库下载</a>、<a href="https://chrome.google.com/webstore/detail/%E4%BD%8E%E4%BB%B7%E9%9B%B7%E8%BE%BE/hgbfabcmmeahnfcodhpompojenmpoiam?hl=zh-CN" hl="zh-CN">低价雷达</a>、<a href="https://chrome.google.com/webstore/detail/b%E7%AB%99%E8%A7%86%E9%A2%91%E4%B8%8B%E8%BD%BD/ohfnohibneicledlcolmfeochkmbecjd?hl=zh-CN&amp;authuser=0" hl="zh-CN" authuser="0">B站视频下载</a></li>
                        <li>熟知JS基本语法与知识，掌握最新ES6语法与API，熟练TypeScript</li>
                    </ul>
                </div>
                <div className={styles.skillItem}>
                    <header className={styles.skillTitle}>web</header>
                    <ul className={styles.skillDesc}>
                        <li>熟知JS基本语法与知识，掌握最新ES6语法与API，熟练TypeScript</li>
                        <li>开发多个浏览器插件<a href="https://chrome.google.com/webstore/detail/%E7%99%BE%E5%BA%A6%E6%96%87%E5%BA%93%E5%85%8D%E8%B4%B9%E4%B8%8B%E8%BD%BD/imjoocoajfjgnabmlbgpcnpieibibhmd?hl=zh-CN&amp;authuser=0" hl="zh-CN" authuser="0">百度文库下载</a>、<a href="https://chrome.google.com/webstore/detail/%E4%BD%8E%E4%BB%B7%E9%9B%B7%E8%BE%BE/hgbfabcmmeahnfcodhpompojenmpoiam?hl=zh-CN" hl="zh-CN">低价雷达</a>、<a href="https://chrome.google.com/webstore/detail/b%E7%AB%99%E8%A7%86%E9%A2%91%E4%B8%8B%E8%BD%BD/ohfnohibneicledlcolmfeochkmbecjd?hl=zh-CN&amp;authuser=0" hl="zh-CN" authuser="0">B站视频下载</a></li>
                        <li>熟知JS基本语法与知识，掌握最新ES6语法与API，熟练TypeScript</li>
                    </ul>
                </div>
                <div className={styles.skillItem}>
                    <header className={styles.skillTitle}>web</header>
                    <ul className={styles.skillDesc}>
                        <li>熟知JS基本语法与知识，掌握最新ES6语法与API，熟练TypeScript</li>
                        <li>开发多个浏览器插件<a href="https://chrome.google.com/webstore/detail/%E7%99%BE%E5%BA%A6%E6%96%87%E5%BA%93%E5%85%8D%E8%B4%B9%E4%B8%8B%E8%BD%BD/imjoocoajfjgnabmlbgpcnpieibibhmd?hl=zh-CN&amp;authuser=0" hl="zh-CN" authuser="0">百度文库下载</a>、<a href="https://chrome.google.com/webstore/detail/%E4%BD%8E%E4%BB%B7%E9%9B%B7%E8%BE%BE/hgbfabcmmeahnfcodhpompojenmpoiam?hl=zh-CN" hl="zh-CN">低价雷达</a>、<a href="https://chrome.google.com/webstore/detail/b%E7%AB%99%E8%A7%86%E9%A2%91%E4%B8%8B%E8%BD%BD/ohfnohibneicledlcolmfeochkmbecjd?hl=zh-CN&amp;authuser=0" hl="zh-CN" authuser="0">B站视频下载</a></li>
                        <li>熟知JS基本语法与知识，掌握最新ES6语法与API，熟练TypeScript</li>
                    </ul>
                </div>
                <div className={styles.skillItem}>
                    <header className={styles.skillTitle}>web</header>
                    <ul className={styles.skillDesc}>
                        <li>熟知JS基本语法与知识，掌握最新ES6语法与API，熟练TypeScript</li>
                        <li>开发多个浏览器插件<a href="https://chrome.google.com/webstore/detail/%E7%99%BE%E5%BA%A6%E6%96%87%E5%BA%93%E5%85%8D%E8%B4%B9%E4%B8%8B%E8%BD%BD/imjoocoajfjgnabmlbgpcnpieibibhmd?hl=zh-CN&amp;authuser=0" hl="zh-CN" authuser="0">百度文库下载</a>、<a href="https://chrome.google.com/webstore/detail/%E4%BD%8E%E4%BB%B7%E9%9B%B7%E8%BE%BE/hgbfabcmmeahnfcodhpompojenmpoiam?hl=zh-CN" hl="zh-CN">低价雷达</a>、<a href="https://chrome.google.com/webstore/detail/b%E7%AB%99%E8%A7%86%E9%A2%91%E4%B8%8B%E8%BD%BD/ohfnohibneicledlcolmfeochkmbecjd?hl=zh-CN&amp;authuser=0" hl="zh-CN" authuser="0">B站视频下载</a></li>
                        <li>熟知JS基本语法与知识，掌握最新ES6语法与API，熟练TypeScript</li>
                    </ul>
                </div>
            </div>
        </CommonModule>
    )
}
